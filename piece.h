#include <iostream>
#include <string>

using namespace std;

using namespace std;

class Piece{
	
	public:
		
		string name;
		string abbreviation; //used to identify pieces easier for moves and FEN
		float value; //relative strength of the piece in a typical position (1 to 9)
		
		
};

class Pawn : public Piece{
	
	public:
		
		Pawn();
		
	
};

class Knight : public Piece{
	
	public:
		
		Knight();
		
	
};


class Bishop : public Piece{
	
	public:
		
		Bishop();
		
	
};

class Rook : public Piece{
	
	public:
		
		Rook();
		
	
};


class Queen : public Piece{
	
	public:
		
		Queen();
		
	
};


class King : public Piece{
	
	public:
		
	King();
		
	
};
