#include "piece.h"
#include <vector>

using namespace std;

class Square{
	
	public:
		
		char rank;
		int file;
		string color;
		bool isEmpty;
		
		Piece* piece;
		
		Square(char rank, int file);
		void addPiece(Piece* piece);		
		void print();
		
};

class Board{
	
	public:
		
		vector<Square*> board;
		
		Board();		
		void print();
		void printGraph();
};

