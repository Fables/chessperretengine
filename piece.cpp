#include "piece.h"

Pawn::Pawn(){name="Pawn";   abbreviation= "P";   value=1;}

Knight::Knight(){name="Knight";   abbreviation= "N";   value=3;}

Bishop::Bishop(){name="Bishop";   abbreviation= "B";   value=3;}

Rook::Rook(){name="Rook";   abbreviation= "R";   value=5;}

Queen::Queen(){name="Queen";   abbreviation= "Q";   value=9;}

King::King(){name="King";   abbreviation= "K";   value=2.5;}
