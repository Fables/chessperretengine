#include "board.h"

using namespace std;

Square::Square(char rank, int file){
	this->rank=rank;
	this->file=file;
	isEmpty=true;
	if ( (rank+file) % 2 == 0) {
		color="black";
	}
	else {
		color="light";
	}		
}

void Square::addPiece(Piece*piece){
	this->piece=piece;
}

void Square::print(){
	cout<<"\nSquare "<<rank<<file;
	if (isEmpty){cout<<" is empty "<<color<<" square";}

}


Board::Board(){
	for (char i='a'; i<='h'; i++){
		for (int j=1; j<=8; j++){
			board.push_back(new Square(i,j));
		}
	}
}

void Board::print(){
	for (int i=0; i<board.size(); i++){
		board[i]->print();
	}
}


void Board::printGraph(){
	for (int i=0; i<board.size() ; i++) {		
		if (board[i]->isEmpty) {
			cout <<"*\t";
		}
		if ((i+1)%8==0) {
			cout <<" "<<board[i]->rank<<" \n";
		}
	}
	cout << "1\t2\t3\t4\t5\t6\t7\t8";
	cout << "\nBoard size: "<<board.size();
}
